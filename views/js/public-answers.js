if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', load)
} else {
    load()
}

function load() {
    let socket = io('https://soviet.solutions/');
    fetch('https://soviet.solutions/api/answers')
        .then(res => res.json())
        .then(data => {
            for (let post of data) {
                genObject(post)
            }
        });
    socket.on('newPublicAnswer', (post) => {
        genObject(post)
    })
}

function genObject(post) {
    let questionsDiv = document.getElementsByClassName('answers')[0];
    let object = document.createElement('div');
    object.className = 'post bg-white my-1 p-1';
    object.innerHTML = `
            <div class="bg-light">
                <img
                        class="round-img"
                        src="${post.user.imgUrl}"
                        alt=""
                >
                <p>Answered By</p>
                <p>${post.user.username}</p>
                <hr>
                <div class="text-primary">
                    ${post.user.rank}
                </div>
                <hr>
                <div>
                    Questions: ${post.user.questions.length}
                    <br>
                    Answers: ${post.user.questionsAnswered}
                </div>
            </div>
            <div>
                <h2>${post.question.title}</h2>
                <p class="markdown">${post.question.description}</p>
                <br>
                <h2>Answer</h2>
                <p class="markdown">${post.answer}</p>
                <a href="/view/question/${post.id}" class="btn btn-primary">Original Question</a>
                <a href="/view/answer/${post.id}" class="btn btn-primary">Answer Link</a>
            </div>
            `;
    questionsDiv.append(object)
}
