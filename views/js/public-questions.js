if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', load)
} else {
    load()
}
function load(){
    let socket = io('https://soviet.solutions/');
    fetch('https://soviet.solutions/api/questions')
        .then(res => res.json())
        .then(data => {
            for (let post of data) {
                genObject(post)
            }
            addMarkdown()
        });
    socket.on('newPublicQuestion', (post) => {
        genObject(post)
        addMarkdown()
    })
}

function genObject(post){
    let questionsDiv = document.getElementsByClassName('questions')[0];
    let object = document.createElement('div');
    object.className = 'post bg-white my-1 p-1';
    object.innerHTML = `
            <div class="bg-light">
                <img
                        class="round-img"
                        src="${post.user.imgUrl}"
                        alt=""
                >
                <p>${post.user.username}</p>
                <hr>
                <div class="text-primary">
                    ${post.user.rank}
                </div>
                <hr>
                <div>
                    Questions: ${post.user.questions.length}
                    <br>
                    Answers: ${post.user.questionsAnswered}
                </div>
            </div>
            <div>
                <h2>${post.title}</h2>
                <p class="markdown">${post.description}</p>
                <br>
                <a href="/request/answer/${post.id}" class="btn btn-primary">Submit an answer</a>
                <a href="/view/question/${post.id}" class="btn btn-primary">Question Link</a>
            </div>
            `;
    questionsDiv.append(object)
}

function addMarkdown() {
    let markdownAreas = document.getElementsByClassName('markdown');
    let markdownIt = window.markdownit()
    for(let markdown of markdownAreas) {
        markdown.innerHTML = markdownIt.render(markdown.innerHTML)
        markdown.classList.remove('markdown')
    }
}
