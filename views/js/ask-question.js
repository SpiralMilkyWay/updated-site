if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', load)
} else {
    load()
}
function load() {
    const tabs = document.querySelectorAll('[data-tab-target]');
    const tabContents = document.querySelectorAll('[data-tab-content]');
    const md = window.markdownit();

    tabs.forEach(tab => {
        tab.addEventListener('click', () => {
            const target = document.querySelector(tab.dataset.tabTarget);
            tabContents.forEach(tabContent => {
                tabContent.classList.remove('active')
            });
            tabs.forEach(tab => {
                tab.classList.remove('active')
            });
            if(tab.classList.contains('preview')){
                let previewDiv = document.getElementById('markdown');
                let text = document.getElementById('description');
                previewDiv.innerHTML = md.render(text.value);
                // Add class to code blocks
                let blocks = document.querySelectorAll('pre');
                for(let block of blocks){
                    block.classList.add('prettyprint');
                    block.classList.add('bg-white')
                }
                let title = document.getElementById('title');
                if(!title){title = ''}else{title = title.value}

                document.getElementById('preview-title').innerHTML = title
            }
            tab.classList.add('active');
            target.classList.add('active')
        })
    })
}