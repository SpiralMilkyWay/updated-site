if (document.readyState === 'loading') {
    document.addEventListener('DOMContentLoaded', load)
} else {
    load()
}
function load() {
    let dictatorsSection = document.getElementsByClassName('dictators')[0];
    let commissarsSection = document.getElementsByClassName('commissars')[0];

    fetch('https://soviet.solutions/api/administration')
        .then(res => res.json())
        .then(users => {
            for (let data of users) {
                console.log(data);
                let object = document.createElement('div');
                object.className = 'profile bg-light';
                object.innerHTML = `
            <img class="round-img" src=${data.imgUrl} alt="Profile Image">

            <div>
                <h2>${data.username}</h2>
                <small>${data.alias ? data.alias : 'None'}</small>
                <p><i>${data.quote ? data.quote : 'No Quote'}</i></p>
                <a href="/profile/${data.id}" class="btn btn-primary">Visit Profile</a>
            </div>

            <ul>
                <li>Discord Account: ${data.discord ? data.discord : 'None'}</li>
                <li>Github: ${data.github ? data.github : 'None'}</li>
                <li>Public Email: ${data.publicEmail ? data.publicEmail : 'None'}</li>
                <li>Website: ${data.website ? data.website : 'None'}</li>
            </ul>
            `;
                if (data.rankNum === 4) {
                    dictatorsSection.append(object)
                } else {
                    commissarsSection.append(object)
                }
            }
        });
}
