const express = require('express');
const mongoose = require('mongoose');
const flash = require('express-flash');
const session = require('express-session');
const passport = require('passport');
const methodOverride = require('method-override');

const initPassport = require('./passport-config');

// Server Things
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

require('dotenv').config();

mongoose.connect(process.env.MONGODBCONNECTION, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => console.log('The database is online'));

app.use('',express.static(`${__dirname}/views`));

app.use(express.urlencoded({extended:false}));
app.use(flash());
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave:false,
    saveUninitialized:false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(methodOverride('_method'));
app.set('view engine','ejs');
app.engine('html', require('ejs').renderFile);

initPassport(passport);

app.use('/api',require('./routes/api/app'));
app.use('/delete',require('./routes/delete/app'));
app.use('/edit',require('./routes/edit/app'));
app.use('/forum',require('./routes/forums/app'));
app.use('/view',require('./routes/view/app'));
app.use('/request',require('./routes/request/app'));
app.use('/',require('./routes/no-starter/app'));

app.use(function (req, res, next) {
    res.status(404).render('error',{
        errorCode: '404',
        errorMessage: "The page you requested doesn't exist or was deleted."
    })
});

app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.status(500).render('error',{
        errorCode: '500',
        errorMessage: 'Something went wrong on our end, try again later.'
    })
});

// For live things, not much here
io.on('connection', function(socket){
    socket.on('newPublicQuestion', (question) => {
        if(!question.auth || !question.auth === process.env.INTERNAL_SECRET){return socket.send('error','Idiot')}
        question.auth = null;
        io.emit('questionsUpdate', question);
    });
    socket.on('newPublicAnswer', (answer) => {
        if(!question.auth || !question.auth === process.env.INTERNAL_SECRET){return socket.send('error','Idiot')}
        answer.auth = null;
        io.emit('answerUpdate', answer);
    });
});

server.listen(80,() => {console.log('Server is online!')});

module.exports = server;

