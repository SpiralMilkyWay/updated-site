const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const user = require('./routes/_deps/models/user');

function initialize(passport) {
    const authenticateUser = async (email, password, done) => {
        user.findOne({
            email:email
        }, async (err,user) => {
            if (err){console.error(err)}
            if(!user) {
                return done(null, false, {message: 'Invalid Email Or Password'})
            }else{
                try {
                    if (await bcrypt.compare(password, user.password)) {
                        return done(null, user)
                    } else {
                        return done(null, false, {message: 'Invalid Email Or Password'})
                    }
                } catch (e) {
                    return done(e)
                }
            }
        });
    };

    passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser));
    passport.serializeUser((user, done) => done(null, user.id));
    passport.deserializeUser((id, done) => {
        return done(null, () => {
            user.findOne({
                id:id
            }, (err,user) => {
                if (err){console.error(err)}
                return user
            })
        })
    })
}

module.exports = initialize;