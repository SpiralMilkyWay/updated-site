const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const user = require('../_deps/models/user');
const passport = require('passport');
const unirest = require('unirest');
const emailValidator = require('email-validator')

router.get('/',(req,res) => {
    res.render('index.html')
});

router.get('/forum',(req,res) => {
    res.render('forums.html')
});

router.get('/instructions',(req,res) => {
    res.render('instructions.html')
});

router.get('/administration',(req,res) => {
    res.render('administration.html')
});

router.get('/profile',checkAuthenticated,(req,res) => {
    user.findOne({
        id: req._passport.session.user
    }, async (err,user) => {
        if (err){console.error(err)}
        user.email = null;
        user.tokens = null;
        user.password = null;
        for(let question of user.questions){
            if(question.private){
                question = null;
            }
        }
        for(let answer of user.answers){
            if(answer.private){
                answer = null;
            }
        }
        res.render('profile',{user:user})
    });
});

router.get('/profile/:id',(req,res) => {
    if(req.params.id){
        user.findOne({
            id: req.params.id
        }, async (err,user) => {
            if (err){console.error(err)}
            if(!user){
                res.redirect('/profile')
            }
            user.email = null;
            user.tokens = null;
            user.password = null;
            for(let question of user.questions){
                if(question.private){
                    question = null;
                }
            }
            for(let answer of user.answers){
                if(answer.private){
                    answer = null;
                }
            }
            res.render('profile',{user:user})
        });
    }else{
        res.redirect('/profile')
    }
});

router.get('/dashboard',checkAuthenticated,(req,res) => {
    user.findOne({
        id:req._passport.session.user
    }, async (err,User) => {
        if (err){console.error(err)}
        res.render('dashboard',{user: User})
    });
});

router.get('/register',checkNotAuthenticated,(req,res) => {
    res.render('register')
});

router.get('/login',checkNotAuthenticated,(req,res) => {
    res.render('login')
});

router.post('/register', checkNotAuthenticated,async (req,res) => {
    let errors = []
    let {username,email,password,password2} = req.body
    email = email.toLowerCase()
    try {
        if(req.body['g-recaptcha-response'] === ''){
            return res.render('register',{
                errors: ['Captcha not filled in!'],
                username: username,
                email: email,
                password: password,
                password2: password2
            })
            // We return since we can't continue without the captcha being filled
        }
        if(!(username || email || password || password2)){errors.push('Fill in all fields!')}
        if(password != password2){errors.push('Passwords do not match')}
        if(password.length < 6){errors.push('Password needs to be at least 6 characters')}
        if(!emailValidator.validate(email)){errors.push('Invalid email!')}
        unirest
            .post('https://www.google.com/recaptcha/api/siteverify')
            .send({ "secret": process.env.CAPTCHA_TOKEN, "response": req.body['g-recaptcha-response'] })
            .then((response) => {
                if(!response.body.success){
                    errors.push('Captcha Failed!')
                }
                if(errors.length > 0){
                    return res.render('register',{
                        errors: errors,
                        username: username,
                        email: email,
                        password: password,
                        password2: password2
                    })
                }
                user.findOne({
                    email:req.body.email
                }, async (err,User) => {
                    if (err){console.error(err)}
                    if(User){
                        errors.push('A user with that email already exists!')
                        return res.render('register',{
                            errors: errors,
                            username: username,
                            email: email,
                            password: password,
                            password2: password2
                        })
                    }
                    const hashed = await bcrypt.hash(req.body.password,10);

                    let newUser = new user({
                        id: Date.now().toString(),
                        email: email,
                        username: username,
                        password: hashed
                    });
                    await newUser.save();
                    return res.redirect('/login')
                });
            });
    } catch(err) {
        console.error(err)
        res.redirect('/register')
    }
});

router.post('/login',checkNotAuthenticated,passport.authenticate('local',{
    successRedirect: '/dashboard',
    failureRedirect: '/login',
    failureFlash: true
}));

router.delete('/logout',(req,res) => {
    req.logOut();
    res.redirect('/login')
});

function checkAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}
function checkNotAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return res.redirect('/dashboard')
    }
    next()
}

module.exports = router;