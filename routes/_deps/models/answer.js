const mongoose = require('mongoose');
const answer = mongoose.model('answer',new mongoose.Schema({
    user: Object,
    answer: String,
    questionId: String,
    id: String,
    questionId: String,
    private: {type: Boolean, default: false},
}));

module.exports = answer;