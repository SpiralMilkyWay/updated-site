const mongoose = require('mongoose');
const question = mongoose.model('question',new mongoose.Schema({
    user: Object,
    id: String,
    title: String,
    description: String,
    private: {type: Boolean, default: false},
    // These are answers that non-admins have submitted
    potentialAnswers: {type: Array, default: []},
    answer: {type: Number, default: null}
}));

module.exports = question;