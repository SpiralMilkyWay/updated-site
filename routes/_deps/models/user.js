const mongoose = require('mongoose');
const user = mongoose.model('user',new mongoose.Schema({
    username: String,
    id: String,
    email: String,
    password: String,
    administrator: {type: Boolean, default: false},
    alias: {type: String, default: 'None'},
    quote: {type: String, default: 'None'},
    about: {type: String, default: 'None'},
    rank: {type: String, default: 'Comrade'},
    rankNum: {type: Number, default: 0},
    imgUrl : {type: String, default: 'https://i.imgur.com/ypwLDvN.jpg'},
    website: {type: String, default: ''},
    github: {type: String, default: ''},
    discord: {type: String, default: ''},
    publicEmail: {type: String, default: ``},
    questions: {type: Array, default: []},
    answers: {type: Array, default: []},
    questionsAnswered: {type: Number, default: 0},
    tokens: {type: Array, default: []},
}));


module.exports = user;