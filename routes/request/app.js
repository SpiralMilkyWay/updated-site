const express = require('express');
const router = express.Router();
const questions = require('../_deps/models/question');
const users = require('../_deps/models/user');
const answers = require('../_deps/models/answer')

router.get('/question',checkAuthenticated,(req,res) => {
    res.render('ask-question.html')
});

router.get('/answer/:id',checkAuthenticated,(req,res) => {
    if(!req.params.id){return res.redirect('/view/question/all')}
    questions.findOne({
        id: req.params.id
    },(err,question) => {
        if(err){console.error(err)}
        if(!question){return res.redirect('/view/question/all')}
        res.render('answer-question',{question: question})
    })
});

router.post('/answer/:id',checkAuthenticated,(req,res) => {
    if(!req.params.id){return res.redirect('/view/question/all')}
    users.findOne({
        id: req._passport.session.user
    },(err,user) => {
        if(err){console.error(err)}
        questions.findOne({
            id: req.params.id
        },(err,question) => {
            if(err){console.error(err)}
            if(!question){return res.redirect('/view/question/all')}
            
            let restrictedUser = createRestrictedUser(user)

            let id = Date.now().toString()
            let newAnswer = new answers({
                user: restrictedUser.user,
                answer: req.body.description,
                questionId: question.id,
                id: id,
                private: question.private
            })
            if(user.administrator){
                question.answer = question.id
                newAnswer.id = question.id
            } else {
                question.potentialAnswers.push(id)
            }

            newAnswer.save()
            question.save()

            user.answers.push({
                title: req.body.title,
                description: req.body.description,
                answer: req.body.description,
                id: id
            })

            user.save()

            res.redirect('/view/question/all')
        })
    })
})

router.post('/question',checkAuthenticated,(req,res) => {
    users.findOne({
        id: req._passport.session.user
    },(err,user) => {
        if(err){console.error(err)}

        let restrictedUser = createRestrictedUser(user)

        let private = false
        if(req.body.private === 'on'){private = true}
        let id = Date.now().toString()
        let newQuestion = new questions({
            user: restrictedUser.user,
            id: id,
            title: req.body.title,
            description: req.body.description,
            private: private
        })
        newQuestion.save()

        user.questions.push({
            title: req.body.title,
            description: req.body.description,
            id: id
        })

        user.save()

        res.redirect('/dashboard')
    })
});

function checkAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}

function createRestrictedUser(user) {
    let importantData = []
    importantData.push({password: user.password})
    importantData.push({email: user.email})
    importantData.push({tokens: user.tokens})
    
    user.email = '[REDACTED]';
    user.tokens = [];
    user.password = '[REDACTED]';
    for(let question of user.questions){
        if(question.private){
            question = '[REDACTED]';
        }
    }
    for(let answer of user.answers){
        if(answer.private){
            answer = '[REDACTED]';
        }
    }
    return {user: user, importantInfo: importantData}
}


module.exports = router;