const express = require('express');
const router = express.Router();
const users = require('../_deps/models/user');
const questions = require('../_deps/models/question')
const answers = require('../_deps/models/answer')
const bcrypt = require('bcrypt')

router.get('/profile',checkAuthenticated,(req,res) => {
    res.render('delete-profile')
})

router.post('/profile',checkAuthenticated,(req,res) => {
    let password = req.body.password
    let id = req._passport.session.user

    if(!password){res.render('delete-profile',{errors: ['No Password Inputted!'],password: password})}
    // Public questions remain so other people can have the solution
    // Before we delete their profile we delete their questions since the user contains the question id's
    users.findOne({
        id: id
    }, async (err,user) => {
        if(err){console.error(err)}
        if(!await bcrypt.compare(password, user.password)){
            return res.render('delete-profile',{errors: ['Incorrect Password!'],password: password})
        }
        req.logOut();
        res.redirect('/register')
        for(let question of user.questions) {
            questions.findOne({
                id: question.id
            },(err,question) => {
                if(err){console.error(err)}
                if(question.private) {
                    // Private questions only have 1 answer
                    answers.findOne({
                        id: question.id // Since the approved answer shares id we can just search it up
                    },(err,answer) => {
                        if(err){console.error(err)}
                        if(answer){
                            // If it has no answer do nothing
                            answers.deleteOne({id: answer.id})
                        }
                    })
                    questions.deleteOne({d: question.id})
                }
            })
        }
        user.remove()
    })
})

function checkAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}

module.exports = router;