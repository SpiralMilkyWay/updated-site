const express = require('express');
const router = express.Router();
const users = require('../_deps/models/user');
const questions = require('../_deps/models/question');
const answers = require('../_deps/models/answer')

router.get('/administration',(req,res) => {
    let admins = [];
    users.find({
        administrator:true
    }, async (err,Users) => {
        if (err){console.error(err)}
        for(let user of Users) {
            user.email = null;
            user.tokens = null;
            user.password = null;
            for(let question of user.questions){
                if(question.private){
                    question = null;
                }
            }
            for(let answer of user.answers){
                if(
                    answer.private){
                    answer = null;
                }
            }
            // Now all private information is null
            admins.push(user);
        }
        await res.json(admins)
    });
});

router.get('/questions',(req,res) => {
    questions.find({
        private: false
    },(err,questions) => {
        if (err){console.error(err)}
        res.json(questions)
    })
})

router.get('/answers',(req,res) => {
    answers.find({
        private: false
    },(err,answers) => {
        if (err){console.error(err)}
        res.json(answers)
    })
})



module.exports = router;