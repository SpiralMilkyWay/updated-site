const express = require('express');
const router = express.Router();
const users = require('../_deps/models/user');
const questions = require('../_deps/models/question');
const answers = require('../_deps/models/answer')

router.get('/question/:id',(req,res) => {
    if(!req.params.id){res.redirect('/view/question/all')}
    if(req.params.id === 'all'){
        res.render('public-questions.html')
    } else {
        questions.findOne({
            id: req.params.id
        },(err,question) => {
            if(err){console.error(err)}
            if(!question) {
                res.status(404).render('error',{
                    errorCode: '404',
                    errorMessage: "The page you requested doesn't exist or was deleted."
                })
            } else {
                res.render('qna',{
                    question: question
                })
            }
        })
    }
})

router.get('/answer/:id',(req,res) => {
    if(!req.params.id){res.redirect('/view/answer/all')}
    if(req.params.id === 'all'){
        res.render('public-answers.html')
    } else {
        answers.findOne({
            id: req.params.id
        },(err,answer) => {
            if(err){console.error(err)}
            if(!answer) {
                res.status(404).render('error',{
                    errorCode: '404',
                    errorMessage: "The page you requested doesn't exist or was deleted."
                })
            } else {
                res.render('qna',{
                    answer: answer
                })
            }
        })
    }
})



module.exports = router;