const express = require('express');
const router = express.Router();
const users = require('../_deps/models/user');
const emailValidator = require('email-validator')
const imageValidator = require('is-image-url')
const bcrypt = require('bcrypt')

router.get('/profile',checkAuthenticated,(req,res) => {
    users.findOne({
        id: req._passport.session.user
    },(err,user) => {
        if(err){console.error(err)}
        res.render('edit-profile',{
            alias: user.alias,
            quote: user.quote,
            about: user.about,
            email: user.publicEmail,
            image: user.imgUrl
        })
    })
})

router.post('/profile',checkAuthenticated,(req,res) => {
    let errors = []
    users.findOne({
        id: req._passport.session.user
    },(err,user) => {
        if(err){console.error(err)}
        let image = req.body.image
        let email = req.body.email
        if(!email == '') {
            if(!emailValidator.validate(email)){
                errors.push('Invalid Email')
            }
        }
        if(!image== ''){
            if(!imageValidator(image)){
                errors.push('Invalid Image')
            }
        } else {
            image = 'https://i.imgur.com/ypwLDvN.jpg'
        }

        if(errors.length > 0) {
            return res.render('edit-profile',{
                alias: req.body.alias,
                quote: req.body.quote,
                about: req.body.about,
                email: email,
                image: image
            })
        }

        user.alias = req.body.alias
        user.quote = req.body.quote
        user.about = req.body.about
        user.publicEmail = email
        user.image = image

        user.save()

        res.redirect('/dashboard')
    })
})

router.get('/login',(req,res) => {
    users.findOne({
        id: req._passport.session.user
    },(err,user) => {
        if(err){console.error(err)}
        res.render('edit-login',{
            username: user.username,
            email: user.email
        })
    })
})

router.post('/login',(req,res) => {
    let errors = []
    if(!req.body.password){
        return res.render('edit-login',{
            username: user.username,
            email: user.email,
            errors: ['No Password Inputted!']
        })
    }

    users.findOne({
        id: req._passport.session.user
    }, async (err,user) => {
        if(err){console.error(err)}
        //let success = 
        if(!await bcrypt.compare(req.body.password, user.password)) {
            return res.render('edit-login',{
                username: user.username,
                email: user.email,
                errors: ['Incorrect Password!']
            })
        }

        if(!emailValidator.validate(req.body.email)){
            errors.push('Invalid Email')
        } else {
            users.findOne({
                email: req.body.email
            },(err,foundUser) => {
                if(err){console.error(err)}
                if(!foundUser){
                    user.email = req.body.email
                }
                if(!foundUser === user){
                    errors.push('User With Email Already Exists')
                }
            })
        }

        if(!req.body.newPassword == ''){
            if(req.body.newPassword.length < 6){
                errors.push('Password Must Be Longer Then 6 Characters')
            } else {
                const hashed = await bcrypt.hash(req.body.newPassword,10);
                user.password = hashed
            }
        }

        if(req.body.username.length < 3){
            errors.push('Username Needs To Be Longer Then 3 Characters')
        } else {
            user.username = req.body.username
        }

        if(errors.length > 0){
            return res.render('edit-login',{
                username: user.username,
                email: user.email,
                errors: errors
            })
        }

        user.save()
        res.redirect('/dashboard')
    })
})

function checkAuthenticated(req,res,next){
    if(req.isAuthenticated()){
        return next()
    }
    res.redirect('/login')
}

function createRestrictedUser(user) {
    let importantData = []
    importantData.push({password: user.password})
    importantData.push({email: user.email})
    importantData.push({tokens: user.tokens})
    
    user.email = '[REDACTED]';
    user.tokens = [];
    user.password = '[REDACTED]';
    for(let question of user.questions){
        if(question.private){
            question = '[REDACTED]';
        }
    }
    for(let answer of user.answers){
        if(answer.private){
            answer = '[REDACTED]';
        }
    }
    return {user: user, importantInfo: importantData}
}


module.exports = router;